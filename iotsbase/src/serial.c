// Paul Yang
// 2016

#include "FreeRTOS.h"
#include "serial.h"
#include "queue.h"

#include "stm32l4xx.h"
#include "main.h"

/*-----------------------------------------------------------*/

/* Misc defines. */
#define serINVALID_QUEUE				( ( QueueHandle_t ) 0 )
#define serNO_BLOCK						( ( TickType_t ) 0 )
/*-----------------------------------------------------------*/

/* The queue used to hold received characters. */
static QueueHandle_t xRxedChars;
static QueueHandle_t xCharsForTx;

/*-----------------------------------------------------------*/


xComPortHandle xSerialPortInitMinimal( unsigned long ulWantedBaud, unsigned portBASE_TYPE uxQueueLength )
{
	xComPortHandle xReturn;

	/* Create the queues used to hold Rx/Tx characters. */
	xRxedChars = xQueueCreate( uxQueueLength, ( unsigned portBASE_TYPE ) sizeof( signed char ) );
	xCharsForTx = xQueueCreate( uxQueueLength + 1, ( unsigned portBASE_TYPE ) sizeof( signed char ) );

	/* If the queues were created correctly then setup the serial port
		hardware. */
	if( ( xRxedChars != serINVALID_QUEUE ) && ( xCharsForTx != serINVALID_QUEUE ) )
	{
		// FIXME: init hw
	}
	else
	{
		xReturn = ( xComPortHandle ) 0;
	}
	return xReturn;
}

xComPortHandle xSerialPortInit( eCOMPort ePort, eBaud eWantedBaud, eParity eWantedParity, eDataBits eWantedDataBits, eStopBits eWantedStopBits, unsigned portBASE_TYPE uxBufferLength )
{
	return NULL;
}

void vSerialPutString( xComPortHandle pxPort, const signed char * const pcString, unsigned short usStringLength )
{
	signed char *pxNext;

	/* A couple of parameters that this port does not use. */
	( void ) usStringLength;
	( void ) pxPort;

	/* NOTE: This implementation does not handle the queue being full as no
	block time is used! */

	/* The port handle is not required as this driver only supports UART1. */
	( void ) pxPort;

	/* Send each character in the string, one at a time. */
	pxNext = ( signed char * ) pcString;
	while( *pxNext )
	{
		xSerialPutChar( pxPort, *pxNext, serNO_BLOCK );
		pxNext++;
	}
}

signed portBASE_TYPE xSerialGetChar( xComPortHandle pxPort, signed char *pcRxedChar, TickType_t xBlockTime )
{
	/* The port handle is not required as this driver only supports one port. */
	( void ) pxPort;

	/* Get the next character from the buffer.  Return false if no characters
	are available, or arrive before xBlockTime expires. */
	if( xQueueReceive( xRxedChars, pcRxedChar, xBlockTime ) )
	{
		return pdTRUE;
	}
	else
	{
		return pdFALSE;
	}
}

signed portBASE_TYPE xSerialPutChar( xComPortHandle pxPort, signed char cOutChar, TickType_t xBlockTime )
{
	signed portBASE_TYPE xReturn;

	if( xQueueSend( xCharsForTx, &cOutChar, xBlockTime ) == pdPASS )
	{
		xReturn = pdPASS;
		// Enable interrupt to send out data

		/* Enable TXE interrupt */
		LL_USART_EnableIT_TXE(USARTx);
	}
	else
	{
		xReturn = pdFAIL;
	}

	return xReturn;
}

portBASE_TYPE xSerialWaitForSemaphore( xComPortHandle xPort )
{
	return 0;
}

void vSerialClose( xComPortHandle xPort )
{

}

/**
  * @brief  Rx Transfer completed callback
  * @note   This example shows a simple way to report end of IT Rx transfer, and
  *         you can add your own implementation.
  * @retval None
  */
void UART_CharReception_Callback(void)
{
	/* Read Received character. RXNE flag is cleared by reading of RDR register */
	BaseType_t xTaskWokenByReceive = pdFALSE;
	uint8_t rxChar = LL_USART_ReceiveData8(USARTx);
	xQueueSendFromISR(xRxedChars, &rxChar,  &xTaskWokenByReceive);
}

/**
  * @brief  Function called for achieving next TX Byte sending
  * @retval None
  */
void UART_TXEmpty_Callback(void)
{
	BaseType_t xTaskWokenByReceive = pdFALSE;
	signed char cOutChar;

	if( xQueueReceiveFromISR( xCharsForTx, ( void * ) &cOutChar, &xTaskWokenByReceive) == pdTRUE)
	{
		/* Fill TDR with a new char */
		LL_USART_TransmitData8(USARTx, cOutChar);
	}
	else
	{
		/* Disablbe TXE interrupt */
		LL_USART_DisableIT_TXE(USARTx);

		/* Enable TC interrupt */
		LL_USART_EnableIT_TC(USARTx);
	}
}

/**
  * @brief  Function called at completion of last byte transmission
  * @retval None
  */
void UART_CharTransmitComplete_Callback(void)
{
	/* Disable TC interrupt */
	LL_USART_DisableIT_TC(USARTx);
}

/**
  * @brief  UART error callbacks
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void UART_Error_Callback(void)
{
  __IO uint32_t isr_reg;

  /* Disable USARTx_IRQn */
  NVIC_DisableIRQ(USARTx_IRQn);

  /* Error handling example :
    - Read USART ISR register to identify flag that leads to IT raising
    - Perform corresponding error handling treatment according to flag
  */
  isr_reg = LL_USART_ReadReg(USARTx, ISR);


}
